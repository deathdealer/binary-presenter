﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryPresenter
{
    public interface IInterpretation //<T> where T : struct
    {
        int ToNumber(string text);
        string ToText(int number);
    }

    //public class Interpretations //<T> where T : struct
    //{
    public class DecSigned : IInterpretation //<T>
    {
        public int ToNumber(string text)
        {
            return short.Parse(text);
        }

        public string ToText(int number)
        {
            return ((short)number).ToString();
        }
    }

    public class DecUnsigned : IInterpretation
    {
        public int ToNumber(string text)
        {
            return ushort.Parse(text);
        }

        public string ToText(int number)
        {
            return ((ushort)number).ToString();
        }
    }

    public class Hex : IInterpretation
    {
        public int ToNumber(string text)
        {
            return ushort.Parse(text, System.Globalization.NumberStyles.HexNumber);
        }

        public string ToText(int number)
        {
            return ((ushort)number).ToString("X");
        }
    }

    public class Bin : IInterpretation
    {
        public int ToNumber(string text)
        {
            return Convert.ToUInt16(text, 2);
        }

        public string ToText(int number)
        {
            return Convert.ToString(((ushort)number), 2); ;
        }
    }

    public class Character : IInterpretation
    {
        public int ToNumber(string text)
        {
            var array = Encoding.UTF8.GetBytes(text).Reverse().Where(b => b != 0).ToArray();
            return BitConverter.ToInt16(array.Length < 2 ? array.Concat(new[] { (byte)0 }).ToArray() : array, 0);
        }

        public string ToText(int number)
        {
            return Encoding.UTF8.GetString(BitConverter.GetBytes((short)number).Where(b => b != 0).Reverse().ToArray());
        }
    }

    public class Bcd : IInterpretation
    {
        public int ToNumber(string text)
        {
            return ushort.Parse(short.Parse(text).ToString(), System.Globalization.NumberStyles.HexNumber);
        }

        public string ToText(int number)
        {
            short bcd;
            if (short.TryParse(((ushort)number).ToString("X"), out bcd))
                return bcd.ToString();
            else
                return "----";
        }
    }

    //}
}
