﻿namespace BinaryPresenter
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MainTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.CharTextBox = new System.Windows.Forms.TextBox();
            this.CharLabel = new System.Windows.Forms.Label();
            this.BinTextBox = new System.Windows.Forms.TextBox();
            this.BinLabel = new System.Windows.Forms.Label();
            this.HexTextBox = new System.Windows.Forms.TextBox();
            this.HexLabel = new System.Windows.Forms.Label();
            this.BcdTextBox = new System.Windows.Forms.TextBox();
            this.BcdLabel = new System.Windows.Forms.Label();
            this.DecUnsignedTextBox = new System.Windows.Forms.TextBox();
            this.DecUnsignedLabel = new System.Windows.Forms.Label();
            this.DecSignedLabel = new System.Windows.Forms.Label();
            this.DecSignedTextBox = new System.Windows.Forms.TextBox();
            this.MainTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTableLayoutPanel
            // 
            this.MainTableLayoutPanel.AutoSize = true;
            this.MainTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.MainTableLayoutPanel.ColumnCount = 2;
            this.MainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.MainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MainTableLayoutPanel.Controls.Add(this.CharTextBox, 1, 5);
            this.MainTableLayoutPanel.Controls.Add(this.CharLabel, 0, 5);
            this.MainTableLayoutPanel.Controls.Add(this.BinTextBox, 1, 0);
            this.MainTableLayoutPanel.Controls.Add(this.BinLabel, 0, 0);
            this.MainTableLayoutPanel.Controls.Add(this.HexTextBox, 1, 1);
            this.MainTableLayoutPanel.Controls.Add(this.HexLabel, 0, 1);
            this.MainTableLayoutPanel.Controls.Add(this.BcdTextBox, 1, 4);
            this.MainTableLayoutPanel.Controls.Add(this.BcdLabel, 0, 4);
            this.MainTableLayoutPanel.Controls.Add(this.DecUnsignedTextBox, 1, 3);
            this.MainTableLayoutPanel.Controls.Add(this.DecUnsignedLabel, 0, 3);
            this.MainTableLayoutPanel.Controls.Add(this.DecSignedLabel, 0, 2);
            this.MainTableLayoutPanel.Controls.Add(this.DecSignedTextBox, 1, 2);
            this.MainTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.MainTableLayoutPanel.Name = "MainTableLayoutPanel";
            this.MainTableLayoutPanel.RowCount = 6;
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MainTableLayoutPanel.Size = new System.Drawing.Size(284, 262);
            this.MainTableLayoutPanel.TabIndex = 0;
            // 
            // CharTextBox
            // 
            this.CharTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CharTextBox.Location = new System.Drawing.Point(88, 133);
            this.CharTextBox.Name = "CharTextBox";
            this.CharTextBox.Size = new System.Drawing.Size(193, 20);
            this.CharTextBox.TabIndex = 5;
            this.CharTextBox.Tag = "Character";
            // 
            // CharLabel
            // 
            this.CharLabel.AutoSize = true;
            this.CharLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CharLabel.Location = new System.Drawing.Point(3, 130);
            this.CharLabel.Name = "CharLabel";
            this.CharLabel.Size = new System.Drawing.Size(79, 132);
            this.CharLabel.TabIndex = 11;
            this.CharLabel.Text = "Char";
            this.CharLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BinTextBox
            // 
            this.BinTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BinTextBox.Location = new System.Drawing.Point(88, 3);
            this.BinTextBox.Name = "BinTextBox";
            this.BinTextBox.Size = new System.Drawing.Size(193, 20);
            this.BinTextBox.TabIndex = 0;
            this.BinTextBox.Tag = "Bin";
            // 
            // BinLabel
            // 
            this.BinLabel.AutoSize = true;
            this.BinLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BinLabel.Location = new System.Drawing.Point(3, 0);
            this.BinLabel.Name = "BinLabel";
            this.BinLabel.Size = new System.Drawing.Size(79, 26);
            this.BinLabel.TabIndex = 6;
            this.BinLabel.Text = "Bin";
            this.BinLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // HexTextBox
            // 
            this.HexTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HexTextBox.Location = new System.Drawing.Point(88, 29);
            this.HexTextBox.Name = "HexTextBox";
            this.HexTextBox.Size = new System.Drawing.Size(193, 20);
            this.HexTextBox.TabIndex = 1;
            this.HexTextBox.Tag = "Hex";
            // 
            // HexLabel
            // 
            this.HexLabel.AutoSize = true;
            this.HexLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HexLabel.Location = new System.Drawing.Point(3, 26);
            this.HexLabel.Name = "HexLabel";
            this.HexLabel.Size = new System.Drawing.Size(79, 26);
            this.HexLabel.TabIndex = 7;
            this.HexLabel.Text = "Hex";
            this.HexLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BcdTextBox
            // 
            this.BcdTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BcdTextBox.Location = new System.Drawing.Point(88, 107);
            this.BcdTextBox.Name = "BcdTextBox";
            this.BcdTextBox.Size = new System.Drawing.Size(193, 20);
            this.BcdTextBox.TabIndex = 4;
            this.BcdTextBox.Tag = "Bcd";
            // 
            // BcdLabel
            // 
            this.BcdLabel.AutoSize = true;
            this.BcdLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BcdLabel.Location = new System.Drawing.Point(3, 104);
            this.BcdLabel.Name = "BcdLabel";
            this.BcdLabel.Size = new System.Drawing.Size(79, 26);
            this.BcdLabel.TabIndex = 10;
            this.BcdLabel.Text = "BCD";
            this.BcdLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DecUnsignedTextBox
            // 
            this.DecUnsignedTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DecUnsignedTextBox.Location = new System.Drawing.Point(88, 81);
            this.DecUnsignedTextBox.Name = "DecUnsignedTextBox";
            this.DecUnsignedTextBox.Size = new System.Drawing.Size(193, 20);
            this.DecUnsignedTextBox.TabIndex = 3;
            this.DecUnsignedTextBox.Tag = "DecUnsigned";
            // 
            // DecUnsignedLabel
            // 
            this.DecUnsignedLabel.AutoSize = true;
            this.DecUnsignedLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DecUnsignedLabel.Location = new System.Drawing.Point(3, 78);
            this.DecUnsignedLabel.Name = "DecUnsignedLabel";
            this.DecUnsignedLabel.Size = new System.Drawing.Size(79, 26);
            this.DecUnsignedLabel.TabIndex = 9;
            this.DecUnsignedLabel.Text = "Dec (unsigned)";
            this.DecUnsignedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DecSignedLabel
            // 
            this.DecSignedLabel.AutoSize = true;
            this.DecSignedLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DecSignedLabel.Location = new System.Drawing.Point(3, 52);
            this.DecSignedLabel.Name = "DecSignedLabel";
            this.DecSignedLabel.Size = new System.Drawing.Size(79, 26);
            this.DecSignedLabel.TabIndex = 8;
            this.DecSignedLabel.Text = "Dec (signed)";
            this.DecSignedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DecSignedTextBox
            // 
            this.DecSignedTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DecSignedTextBox.Location = new System.Drawing.Point(88, 55);
            this.DecSignedTextBox.Name = "DecSignedTextBox";
            this.DecSignedTextBox.Size = new System.Drawing.Size(193, 20);
            this.DecSignedTextBox.TabIndex = 2;
            this.DecSignedTextBox.Tag = "DecSigned";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.MainTableLayoutPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Binary presenter";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MainTableLayoutPanel.ResumeLayout(false);
            this.MainTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel MainTableLayoutPanel;
        private System.Windows.Forms.TextBox BcdTextBox;
        private System.Windows.Forms.Label BcdLabel;
        private System.Windows.Forms.TextBox DecUnsignedTextBox;
        private System.Windows.Forms.Label DecUnsignedLabel;
        private System.Windows.Forms.Label DecSignedLabel;
        private System.Windows.Forms.TextBox DecSignedTextBox;
        private System.Windows.Forms.TextBox BinTextBox;
        private System.Windows.Forms.Label BinLabel;
        private System.Windows.Forms.TextBox HexTextBox;
        private System.Windows.Forms.Label HexLabel;
        private System.Windows.Forms.TextBox CharTextBox;
        private System.Windows.Forms.Label CharLabel;
    }
}

