﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BinaryPresenter
{
    public partial class MainForm : Form
    {
        private int _number;
        public int Number
        {
            get { return _number; }
            set
            {
                _number = value;
                foreach (var textBox in MainTableLayoutPanel.Controls.OfType<TextBox>().Where(tb => !tb.Focused))
                {
                    textBox.TextChanged -= TextBoxes_TextChanged;
                    var converter = GetConverter(textBox);
                    textBox.Text = converter?.ToText(value);
                    SetTextBoxNormalState(textBox);
                    textBox.TextChanged += TextBoxes_TextChanged;
                }
            }
        }

        public MainForm()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            Number = 0;
        }

        private void TextBoxes_TextChanged(object sender, EventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox != null)
            {
                var converter = GetConverter(sender as TextBox);
                if (converter != null)
                {
                    try
                    {
                        Number = converter.ToNumber(textBox.Text);
                        SetTextBoxNormalState(textBox);
                    }
                    catch (Exception ex)
                    {
                        SetTextBoxErrorState(textBox);
                        //textBox.Text = ex.Message;
                    }
                }
            }
        }

        public /*static*/ IInterpretation/*<int>*/ GetConverter(TextBox textBox)
        {
            try
            {
                var type = this.GetType().Assembly.GetTypes().FirstOrDefault(t => typeof(IInterpretation).IsAssignableFrom(t) && t.IsClass && !t.IsAbstract && t.Name == textBox.Tag as string);
                return (IInterpretation)Activator.CreateInstance(type);
            }
            catch (Exception ex)
            {
                //System.Diagnostics.Debugger.Break();
                return null;
            }
        }

        private static void SetTextBoxErrorState(TextBox textBox)
        {
            textBox.ForeColor = Color.Red;
            textBox.Font = new Font(textBox.Font, FontStyle.Bold);
        }
        private static void SetTextBoxNormalState(TextBox textBox)
        {
            textBox.ForeColor = Color.FromKnownColor(KnownColor.WindowText);
            textBox.Font = new Font(textBox.Font, FontStyle.Regular);
        }
    }
}
